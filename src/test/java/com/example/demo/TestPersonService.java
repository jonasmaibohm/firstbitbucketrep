package com.example.demo;

import com.example.demo.dao.PersonDao;
import com.example.demo.model.Person;
import com.example.demo.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TestPersonService {
    @InjectMocks
    PersonService service;

    @Mock
    PersonDao dao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void selectAllPeopleTest() {
        List<Person> list = new ArrayList<Person>();
        Person personOne = new Person(UUID.randomUUID(),"john");
        Person personTwo = new Person(UUID.randomUUID(),"Bob");
        Person personThree = new Person(UUID.randomUUID(),"Billy");

        list.add(personOne);
        list.add(personTwo);
        list.add(personThree);

        when(dao.selectAllPeople()).thenReturn(list);

        //test
        List<Person> personList = service.getAllPeople();

        assertEquals(3, personList.size());
        verify(dao, times(1)).selectAllPeople();

    }


}
